FROM gtechedu/freeling
LABEL Description="Imagem do Coh-metrix-esp"


#install cometrix espanhol  
RUN apt-get update && apt-get install -y openjdk-8-jdk  && apt-get clean && rm -rf /var/cache/apt/archives /var/lib/apt/lists/*     
#RUN mkdir /usr/cohmetrix && curl -L -o /usr/cohmetrix/coh-metrix-es.jar https://github.com/andreqi/coh-metrix-esp/blob/master/coh-metrix-esp.jar?raw=true


COPY www/. /var/www/html/
WORKDIR /var/www/html

EXPOSE 80
CMD apachectl -D FOREGROUND
<?php

require 'util.php';

class Control
{
    private $dir = '/tmp/';

    public function processText()
    {

        $file = '';
        if (!isset($_REQUEST['text'])) {
            return serviceError('É necessário informar o texto');
        }else{
            $file = $this->dir . uniqid();
            file_put_contents($file, $_REQUEST['text']);
        }

        $exec = 'java -jar program/coh-metrix-esp-0.0.1.jar -i ' . $file . ' -o ' . $file . '.json';
        //echo $exec;
        exec($exec, $output, $returnvar);
        if ($returnvar == 0) {
          while(!file_exists( $file.'.json')){
            sleep(2);
          }
          echo file_get_contents($file.'.json');
        } else {
            $ret = '';
            foreach ($output as $linha) {
                $ret .= $linha;
            }
            return serviceError($ret);

        }
        //unlink($file);
    }
}

$ct = new Control();
$ct->processText();

//processaRequisicao();
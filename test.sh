#!/bin/bash
./build.sh

docker stop test-coh-metrix-esp
docker rm test-coh-metrix-esp
docker run -v $PWD/www:/var/www/html/ -p 8080:80 --name test-coh-metrix-esp gtechedu/coh-metrix-esp